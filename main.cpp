#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include <iostream>
#include <stack>
#include <list>
#include <iomanip>
#include <locale>
#include <sstream>
#include <string>
#include <vector>

//Compiling code clear & g++ main.cpp -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -o drive 

using namespace std;

struct Card{

    sf::Image image;
    sf::Sprite sprite;
    sf::Texture ragnaros;
    int manaCoast;
    int cardDamage;
    int cardHealth;
    bool deathRattle;
    bool warCry;
    sf::Font font;
    sf::Text text;
    int handPosition;
    int boardPosition;

    //sf::Texture third;
    //sf::Sprite third2;

    

    Card(){
        //third.loadFromFile("selection.png");
        //third2.setTexture(third);

        handPosition=0;
        boardPosition=0;
        
        image.loadFromFile("rage.png"); //primeira carta
        image.createMaskFromColor(sf::Color::Blue); //cor alpha , translucida

        ragnaros.loadFromImage(image);
        sprite.setTexture(ragnaros);
        //sprite.setPosition(300+handAmount*40,605);


        sf::Vector2f scale = sprite.getScale();
        sprite.setScale(scale.x/1.5,scale.y/1.5);
        //sprite.setScale(scale.x/1.75,scale.y/2);

        cardDamage = 8;
        cardHealth = 8;
        manaCoast = 8;

        string String = static_cast<ostringstream*>( &(ostringstream() << cardHealth) )->str();//Famoso pulo do gato


        font.loadFromFile("arial.ttf");
        text.setFont(font);
        text.setColor(sf::Color::Red);
        text.setString(String);
        text.setPosition(308,612);

    }
};

class player
{
public:
    Card card;
    int heroType;
    std::stack<Card> Deck; //deck de cada player
    int heroHealth; //hp de cada player
    int heroDamage; //dano de cada player 
    //int armor; // quantidade de armadura de cada player
    std::vector<Card> hand; // cartas na mao de cada player 
    std::list<Card> board; //cartas no tabuleiro de cada player
    bool heroPower; // diz se o poder heroico foi ou nao usado
    int mana; // diz a quantidade de cristais de mana cada um tem
    //int handAmount; //diz a quantidade de cartas que cada player tem
    Card temp[2]; // temporario que vai armazenar a busca pelas cartas certas

    player(){
        //handAmount = 0;
        heroHealth = 30;
        heroDamage = 0;
        mana = 0;
        //hand.push_front(card);
        for(int i=0;i<30;i++){  //inicializacao do deck, 30 cartas
            Deck.push(card);
        }
        drawCard();
        drawCard();
        drawCard();
        drawCard();
        drawCard();
        drawCard();
        /*for(std::list<Card>::iterator it=hand.begin();it != hand.end();it++){
            //window.draw((*it).sprite); //THANKS MURILO
            if((*it).handPosition == 2){
                temp[0] = *it;
            }
            if((*it).handPosition == 6){
                temp[1] = *it;
            }                                            
        }                                                               
        attack(temp[0],temp[1]);                                                    
        */
    }
    void drawCard(){
        //handAmount++;
        Card pulled;
        //pulled.sprite.setPosition(300,605);//(300+hand.size()*40,605);
        pulled = Deck.top();
        
        pulled.sprite.setPosition(300+hand.size()*105,605);
        pulled.text.setPosition(308+hand.size()*105,612);
        //pulled.third2.setPosition(300+hand.size()*105,605);

        pulled.handPosition++;
        Deck.pop();
        hand.push_back(pulled);
        //handPosition++;

    }
    void playCard(Card card){
        //boardPosition++;
        //handPosition--;
        board.push_front(card);
    }
    void attack(Card card1,Card card2){
        card2.cardHealth = card2.cardHealth - card1.cardDamage;
        card1.cardHealth = card1.cardHealth - card2.cardDamage;  
    }
};

int main()
{
    int status = 1;

    //int handAmount=0;

    player PLAYER;
//WELL WELL, FK THIS SCREEN BUG 
here:
    sf::RenderWindow window(sf::VideoMode(1366, 768,24), "SFML works!",sf::Style::Fullscreen);
    if(window.getSize().x != 1366 && window.getSize().y != 768){
        goto here;
    }
    //YOU'RE GODAMN RIGHT
    std::cout<<window.getSize().x << " " << window.getSize().y;


    //window.setFramerateLimit(60);// Movimento das cartas limitado
    sf::CircleShape shape(100.f);//whatevar
    shape.setFillColor(sf::Color::Green);//whatevar


    /////////////////////////////////////////////////////////////////////////////////////
    sf::Image image2;
    image2.loadFromFile("hs.png");
    image2.createMaskFromColor(sf::Color::Blue);

    sf::Texture background;
    sf::Sprite sprote2;

    background.loadFromImage(image2);
    sprote2.setTexture(background);
    //sf::Vector2f scale = sprote2.getScale();
    //sprote2.setScale(scale.x*4.8,scale.y*4.3);

    ////////////////////////////////////////////////////////////////////////////////////
    sf::Texture txt;
    txt.loadFromFile("Paddle Large.png");
    sf::Sprite selected;
    selected.setTexture(txt);
    selected.setPosition(580,220);

    sf::Vector2f scale = selected.getScale();
    selected.setScale(scale.x*0.7, scale.y * 0.4);
    //selected.setScale(scale.x*2.6, scale.y * 1.4);

    ///////////////////////////////////////////////////////////////////////////////////
    sf::Texture tox;
    //tox.loadFromFile("board2.jpg");
    tox.loadFromFile("hearth1.png");
    sf::Sprite board;
    board.setTexture(tox);
    //scale = board.getScale();
    //board.setScale(scale.x*2.4,scale.y*2.18);

    ////////////////////////////////////////////////////////////////////////////////

    sf::Image image;
    image.loadFromFile("rag.png"); //primeira carta
    image.createMaskFromColor(sf::Color::Blue); //cor alpha , translucida

    sf::Texture ragnaros;
    sf::Sprite sprote;

    ragnaros.loadFromImage(image);
    sprote.setTexture(ragnaros);

    scale = sprote.getScale();
    sprote.setScale(scale.x/2,scale.y/2);

    /////////////////////////////////////////////////////////////////////////////
    sf::Texture third;
    sf::Sprite third2;

    third.loadFromFile("selection.png");
    third2.setTexture(third);

    third2.setPosition(300,605);



    sf::Event nope; //Eventos do teclado para 

    int posx=100,posy=100;//posicao do ragnaruszinho

    bool permission = false;//mostra se pode ou nao clicar no play

    bool permission2 = false;//mostra se pode ou nao mecher num boneco



    while (window.isOpen())
    {
        
        sf::Vector2i mouse;


        permission = false;
        permission2 = false;

        sf::Vector2i mouse2(300,300);
        mouse = sf::Mouse::getPosition();

        sprote.setPosition(posx,posy);
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
                window.close();
                   
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
                sf::Mouse::setPosition(mouse2);
            }
                //if(event.type == sf::Event::KeyReleased){
                    //window.close();
                //}
            break;
        }

        /*while(window.waitEvent(nope)){
                
                    switch(nope.type){
                        case sf::Event::KeyReleased:
 
                            switch(nope.key.code){
                                case sf::Keyboard::J:
                                    posx = posx + 100;
                                    std::cout<<"Damn";

                                    break;

                                case sf::Keyboard::G:
                                    posx = posx - 100;
                                    std::cout<<"Wow";

                                    break;

                                case sf::Keyboard::Y:
                                    posy = posy - 100;
                                    std::cout<<"Intensifies";

                                    break; 
                                case sf::Keyboard::H:
                                    posy = posy +100;

                                    break;
                                break;
                            }
                        break;
                    }
                    break;
        }*/
        
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x > 200 ){
            posy = posy + 50;
            posx = posx + 50;
            //std::cout<<mouse.x<<std::endl;
            //std::cout<<mouse.y<<endl;
        }


        if(mouse.x >= 575 && mouse.x < 790 && mouse.y >= 220 && mouse.y < 250 ){//&& sf::Mouse::isButtonPressed(sf::Mouse::Left)){
            //std::cout<<"Cliquei no Botao";
            //window.draw(selected);
            permission = true;
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left) == true){

                status = 2;
            }
        }
        if(status == 2){//TELA 2, MOSTRA QUAL SERIA SELECIONADO
            if(mouse.x >= 300 && mouse.y >= 605 && mouse.x <= 1100){//&& sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                if(mouse.x >= 300 && mouse.y >= 605 && mouse.x < 404 && PLAYER.hand.size() >= 1 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                    third2.setPosition(300,605);
                    permission2 = true;
                    //if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                      //  third2.setPosition(mouse.x,mouse.y);
                    //}
                }// PRIMEIRA POSICAO NA MAO
                if(mouse.x >= 405 && mouse.y >= 605 && mouse.x < 509 && PLAYER.hand.size() >= 2 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//SEGUNDA POSICAO NA MAO
                    third2.setPosition(405,605);
                    permission2 = true;
                }
                if(mouse.x >= 510 && mouse.y >= 605 && mouse.x < 614 && PLAYER.hand.size() >= 3 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//TERCEIRA POSICAO NA MAO
                    third2.setPosition(510,605);
                    permission2 = true;
                }
                if(mouse.x >= 615 && mouse.y >= 605 && mouse.x < 719 && PLAYER.hand.size() >= 4 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//QUARTA POSICAO NA MAO
                    third2.setPosition(615,605);
                    permission2 = true;
                }
                if(mouse.x >= 720 && mouse.y >= 605 && mouse.x < 824 && PLAYER.hand.size() >= 5  && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//QUINTA POSICAO NA MAO
                    third2.setPosition(720,605);
                    permission2 = true;
                }
                if(mouse.x >= 825 && mouse.y >= 605 && mouse.x < 929 && PLAYER.hand.size() >= 6 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//SEXTA POSICAO NA MAO
                    third2.setPosition(825,605);
                    permission2 = true;
                }
                if(mouse.x >= 930 && mouse.y >= 605 && mouse.x < 1034 && PLAYER.hand.size() >= 7 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//SETIMA POSICAO NA MAO
                    third2.setPosition(930,605);
                    permission2 = true;
                }
                if(mouse.x >= 1035 && mouse.y >= 605 && PLAYER.hand.size() >= 8 && !sf::Mouse::isButtonPressed(sf::Mouse::Left)){//OITAVA POSICAO NA MAO
                    third2.setPosition(1035,605);
                    permission2 = true;
                }
            }
            //JOGADA EM SI AO MOVER DA MAO PARA A MESA
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 300 && mouse.y >= 605 && mouse.x <= 1100){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                third2.setPosition(mouse.x,mouse.y);
                permission2 = true;
                //PLAYER.hand.at(0).sprite.setPosition(300,300);
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 300 && mouse.y >= 605 && mouse.x <= 404){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(0).sprite.setPosition(300,300);
                } 
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 405 && mouse.y >= 605 && mouse.x < 509){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(1).sprite.setPosition(300,300);
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 510 && mouse.y >= 605 && mouse.x < 614){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(2).sprite.setPosition(300,300);
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 615 && mouse.y >= 605 && mouse.x < 719){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(3).sprite.setPosition(300,300);
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 720 && mouse.y >= 605 && mouse.x < 824){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(4).sprite.setPosition(300,300);
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 825 && mouse.y >= 605 && mouse.x < 929){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(5).sprite.setPosition(300,300);
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 930 && mouse.y >= 605 && mouse.x < 1034){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);                    
                    PLAYER.hand.at(6).sprite.setPosition(300,300);
                }
                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouse.x >= 1035 && mouse.y >= 605 && PLAYER.hand.size() >= 8){ //FAZENDO A JOGADA DA MAO PARA O CAMPO
                    third2.setPosition(mouse.x,mouse.y);
                    PLAYER.hand.at(7).sprite.setPosition(300,300);
                }
            }

   

        }//92 127
        
        window.clear();
        if(status == 1){ //Caso esteja na primeira tela
            window.draw(sprote2);
            window.draw(shape);
            window.draw(sprote);
            if(permission == true){
                window.draw(selected);
            }
        }
        /*if(mouse.x >= 575 && mouse.x < 790 && mouse.y >= 220 && mouse.y < 250 && sf::Mouse::isButtonPressed(sf::Mouse::Left)){
            //std::cout<<"Cliquei no Botao";
            window.draw(selected);
        }*/
        if(status == 2){
            window.draw(board);
            for(std::vector<Card>::iterator it=PLAYER.hand.begin();it != PLAYER.hand.end();it++){
                window.draw((*it).sprite); //THANKS MURILO
                window.draw((*it).text);
            }

            if(permission2 == true){
                window.draw(third2);
            }
        }

        window.display();
    }

    return 0;
}